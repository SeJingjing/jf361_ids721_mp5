# jf361_ids721_mp5

## Project Introduction
This project is centered on the development of a serverless Rust microservice utilizing AWS Lambda, aimed at delivering 
a simple service with a database integration.

## Project Description
Create a Rust AWS Lambda function

Implement a simple service

Connect to a database

## Project Setup
1. create a simple AWS RUST Lambda function

    1. Use below code to create a sample RUST funciton.
       ```angular2html
       cargo lambda new <PROJECT-NAME>
       cd <PROJECT-NAME>
       ```
    2. Implement the function in `src/main.rs` and add dependencies in `Cargo.toml`.
    
    3. Create a `role` for this project and add permission `AmazonDynamoDBFullAccess`, `AWSLambda_FullAccess`, and 
    `AWSLambdaBasicExecutionRole`
   
2. Create database (Use **DynamoDB** in AWS to create the database) 
   1. Click `create table` and enter the TABLE NAME.
   2. Click `create item` to add elements to the table]

3. Deploy project
   1. Deploy the RUST function project to AWS.
    ```angular2html
        cargo lambda build --release
        cargo lambda deploy --region <AWS-REGION> --iam-role <ROLE-IAM>
    ```
   2. Search for `Lambda` on the AWS console and find the lambda function with name <PROJECT-NAME>
   3. In this function, add a trigger:`API Gateway` with type of `REST API` to it.
   4. Click the created API Gateway, create a new resource for it.
   5. In this new resource, create a  `POST` method and then `enable CORS`
   6. Click `Deploy API` and create a new stage.

4. Test the functionality
   Use the URL under the `API Gateway` that with the resource name at the end.


## Screenshot
### Lambda Funciton and API Gateway
<p align="center">
  <img src="lambda.png" />
</p>

### DynamoDB
<p align="center">
  <img src="DynamoDB.png" />
</p>

table info
<p align="center">
  <img src="table.png" />
</p>

### Service
```
https://o8pksx69lj.execute-api.us-east-1.amazonaws.com/default/mini_project5/mp_resource
```

This service is to provide information search about the courses in Duke University. Using `course_id` to search the 
`course_name` and `semester` offered. I test this service using **Postman**. 
<p align="center">
  <img src="service.png" />
</p>
